package gui;
import java.io.File;

import classStructure.Record;
import databaseManipulation.FileManipulator;

public class GuiManager 

{
	private  String currentFile;
	private  FileManipulator fileHandler;
	private  CreateDatabase db;
	private  CreateRecordGui rec;
	private  DataMethodGui data;
	private  EditRecordGui edit;
	private  DeleteDatabaseGui deleteDb;
	private  DeleteRecordGui deleteRec;
	private  GuiMain main;
	private  OpenDatabaseGui openDb;
	private  SearchRangeGui searchRange;
	private  SearchRecordGui search;
	
	private  String dataBase;
	private  boolean open = false;
	//private  ShowAllGui showAll;
	
	public GuiManager(String dataBase)
	{
		//the size of each individual record
		
		//relative path to database
		this.dataBase = dataBase;
		edit = new EditRecordGui(this);
		db = new CreateDatabase(this);
		rec = new CreateRecordGui(this);
		data = new DataMethodGui(this);
		deleteDb = new DeleteDatabaseGui(this);
		deleteRec = new  DeleteRecordGui(this);
		main = new GuiMain(this);
		openDb = new OpenDatabaseGui(this);
		searchRange = new SearchRangeGui(this);
		search = new SearchRecordGui(this);
		
		
	}
	//these methods change the invisibility of each of the gui windows
	//TODO CHANGE METHODS TO IMPLEMENTS AN INTERFACE for polymorphic behaviour
	public  void visible(Object j,boolean b)
	{
		if(j instanceof Ivisible)
		{
			((Ivisible) j).visible(b);

		}
	}

	
	//allows a new file to be set to handler
	public  void setFileHandler(File file)
	{
	
	 fileHandler = new FileManipulator(file,dataBase, Record.RECORD_SIZE);
	 
	}
	public  void setOpen(boolean open) {
		this.open = open;
	}
	//allows getting of the file handeler if open
	public  FileManipulator getFileHandler()
	{
		if(open)
			return fileHandler;
		else 
			return null;
	}
	//returns the name of dataBase
	
	
	public  void showAllRecVis()
	{
		if(open)
			new ShowAllGui(fileHandler.showAll());
	}
	
	public  String getDatabase()
	{
		return dataBase;
	}
	
	public  CreateDatabase getDb() {
		return db;
	}
	public  CreateRecordGui getRec() {
		return rec;
	}
	public  DataMethodGui getData() {
		return data;
	}
	public  EditRecordGui getEdit() {
		return edit;
	}
	public  DeleteDatabaseGui getDeleteDb() {
		return deleteDb;
	}
	public  DeleteRecordGui getDeleteRec() {
		return deleteRec;
	}
	public  GuiMain getMain() {
		return main;
	}
	public  OpenDatabaseGui getOpenDb() {
		return openDb;
	}
	public  SearchRangeGui getSearchRange() {
		return searchRange;
	}
	public  SearchRecordGui getSearch() {
		return search;
	}
}



