package gui;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import databaseManipulation.DataBaseManipulator;

public class DeleteDatabaseGui extends Frame implements ActionListener,Ivisible
{
	 private JButton delete,menu;
	 private JTextField dbName;
	 private JFrame frame;
	 private GuiManager guiManager;
	 public DeleteDatabaseGui(GuiManager guiManager) 
	 {
		 this.guiManager = guiManager;
		 //container for gui elements
		 frame = new JFrame("FrameDemo");
	     frame.setLayout(new FlowLayout()); 
	     //label
	     frame.add(new JLabel("<html>Database Name:<br></html> ", SwingConstants.CENTER));
	     //allows user to neter database to delete
	     dbName = new JTextField(20);
	     frame.add(dbName);
	     //buttons
	     menu = new JButton("Menu");
	     menu.addActionListener(this);
	     frame.add(menu);
	     delete = new JButton("Delete");
	     delete.addActionListener(this);
	     frame.add(delete);
	     
	     //prevents resizing
	     frame.setResizable(false);
	     //sets the title
	     frame.setTitle("Classic Game Database"); 
	     //sets size of window
	     frame.setSize(400, 400); 
	     //makes it invisible
	     frame.setVisible(false);  
	 }
	 
	 //controls visibility of frame
		@Override
		public void visible(boolean b) {
			dbName.setText("");
			frame.setVisible(b);
			
		}

	@Override
	public void actionPerformed(ActionEvent act) 
	{
		//creates object to compare to see source of event
		Object source = act.getSource();
		
		//brings back to menu
		if(source == menu)
		{
			guiManager.visible(this,false);
			guiManager.visible(guiManager.getMain(),true);
			
		}
		
		if(source == delete)
		{
			//makes sure its a valid database
			if(utility.GuiUtility.validFile(dbName.getText()))
			{
				//trys to deletes database and returns true if succesful
				if(DataBaseManipulator.Delete(dbName.getText(), guiManager.getDatabase()))
				{
				//tells user its deleted
				JOptionPane.showMessageDialog(frame, "Database Deleted");
				//switches to main menu
				guiManager.visible(this,false);
				guiManager.visible(guiManager.getMain(),true);
				}
				else
				 {
					//tells user the database doesnt exist
				 	JOptionPane.showMessageDialog(frame, "DataBase does not exist");
				 }
				 
				
			}
			else
			{
				//tells user if the database name is invalid
				JOptionPane.showMessageDialog(frame, "Name must contain at least one"
						+ " Character and must not contain the fullstop character '.'");
			}
		}
		
		
	}
}
