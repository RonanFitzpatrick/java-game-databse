package gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;














import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;


public class GuiMain extends Frame implements ActionListener,Ivisible
{
	 private JButton create,open,delete,help;
	 private  JFrame frame;
	 private GuiManager guimanager;
	 public GuiMain(GuiManager guimanager) 
	 {
		 this.guimanager = guimanager;
		 //container for Gui elements 
		 frame = new JFrame("FrameDemo");
	     frame.setLayout(new FlowLayout()); 
	     
	     //adds buttons to gui that perform the database options
	      create = new JButton("create"); //submit button
	      create.addActionListener(this);
	      frame.add(create);
	      open = new JButton("open");
	      open.addActionListener(this);
	      frame.add(open);
	      delete = new JButton("delete");
	      delete.addActionListener(this);
	      frame.add(delete);
	
	      //submit button
	      
	     //disallows resizing
	      frame.setResizable(false);
	      //sets title of window
	      frame.setTitle("Classic Game Database"); 
	      //sets size of the window
	      frame.setSize(100, 200);       
	      //default of this window is 
	      frame.setVisible(true);           
	 }
	@Override
	public void actionPerformed(ActionEvent act) 
	{
		Object source = act.getSource();
		//takes users the create a databse
		if(source == create)
		{
			guimanager.visible(this,false);
			guimanager.visible(guimanager.getDb(),true);
			
		}
		//takes user to the open a database
		if(source == open)
		{
			guimanager.visible(this,false);
			guimanager.visible(guimanager.getOpenDb(),true);
		}
		//takes user to delete a database
		if(source == delete)
		{
			guimanager.visible(this,false);
			guimanager.visible(guimanager.getDeleteDb(),true);
			
		}
		
		
	}

	@Override
	public void visible(boolean b) 
	{
		frame.setVisible(b);
	}

}
