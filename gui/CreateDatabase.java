package gui;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class CreateDatabase  extends Frame implements ActionListener,Ivisible
{
	 private JButton create,menu;
	 private JTextField dbName;
	 private JFrame frame;
	 private GuiManager guiManager;
	 public CreateDatabase(GuiManager guiManager) 
	 {
		 this.guiManager = guiManager;
		 //creates Container for all the gui elements to be put into 
		 frame = new JFrame("FrameDemo");
	     frame.setLayout(new FlowLayout());
	     //adds label 
	     frame.add(new JLabel("<html>Database Name:<br></html> ", SwingConstants.CENTER));
	     //adds textbox for user to enter  database name
	     dbName = new JTextField(20);
	     frame.add(dbName);
	     //button to revert to menu
	     menu = new JButton("Menu");
	     //A listener is added to see if its clicked
	     menu.addActionListener(this);
	     frame.add(menu);
	     //button to create record
	     create = new JButton("Create");
	     //listener to check if pressed
	     create.addActionListener(this);
	     frame.add(create);
	     
	     //non resizable becuase elements move when resized
	     frame.setResizable(false);
	     //title of window
	     frame.setTitle("Classic Game Database");
	     //size of current window
	     frame.setSize(400, 400);  
	     //invisible when first created 
	     frame.setVisible(false);  
	 }
	 


	@Override
	public void actionPerformed(ActionEvent act)
	{
		//creates an object to allow comparison of events ive defined
		Object source = act.getSource();
		if(source == menu)
		{
			guiManager.visible(this,false);
			guiManager.visible(guiManager.getMain(),true);
		}
		//code above makes current Gui invisible and and the main menu visible
		
		if(source == create)
		{
			//validation of file name
			if(utility.GuiUtility.validFile(dbName.getText()))
			{
				//double checks it is created
				if(databaseManipulation.DataBaseManipulator.create(dbName.getText(),guiManager.getDatabase()))
				{
					JOptionPane.showMessageDialog(frame, "Creation Succesful");
					guiManager.visible(this,false);
					guiManager.visible(guiManager.getMain(),true);
					
				}
				//alerts user it is created and
				//changes screen back to menu when created
				else
				{
					//box telling the user creation failed
					JOptionPane.showMessageDialog(frame, "Creation Failed, File may already exist");
				}
				
				
			}
			else
			{
				JOptionPane.showMessageDialog(frame, "Name must contain at least one"
						+ " Character and must not contain the fullstop character '.'");
			}
		}
		
	}
	
	public void visible(boolean b) {
		dbName.setText("");
		frame.setVisible(b);
		
	}

}
