package gui;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class SearchRecordGui extends Frame implements ActionListener,Ivisible
{
	 private JButton search,menu;
	 private JTextField regex;
	 private JFrame frame;
	 private JRadioButton name,desc,comp,version,playTime,release;
	 private GuiManager guiManager;
	 private ButtonGroup category;
	public SearchRecordGui(GuiManager guiManager)
	{
		  this.guiManager = guiManager;
		  frame = new JFrame("FrameDemo");
	      frame.setLayout(new FlowLayout()); 
		  regex = new JTextField(60);
		  name = new JRadioButton("Name",true);
		  desc = new JRadioButton("Description",false);
		  comp = new JRadioButton("Company",false);
		  version = new JRadioButton("Version",false);
		  playTime = new JRadioButton("Average Playtime",false);
		  release = new JRadioButton("Release year" ,false);
		  
		  
		  category = new ButtonGroup();
		  category.add(name);
		  category.add(comp);
		  category.add(desc);
		  category.add(version);
		  category.add(playTime);
		  category.add(release);
		  
		  frame.add(new JLabel("Regex", SwingConstants.CENTER));
		  frame.add(regex);
		  
		  frame.add(name);
		  frame.add(desc);
		  frame.add(comp);
		  frame.add(version);	  
		  frame.add(playTime);
		  frame.add(release);
		  
		  
	      search= new JButton("search"); //submit button
	      search.addActionListener(this);
	      frame.add(search);
	      menu = new JButton("menu");
	      menu.addActionListener(this);
	      frame.add(menu);
	      
	      frame.setResizable(false);//stops resizing
	      frame.setTitle("Classic Game Database"); //title of window
	      frame.setSize(700, 500);  //size of window       
	      frame.setVisible(false);   
	      frame.addWindowListener(new WindowAdapter() {
	    	  public void windowClosing(WindowEvent e) 
	    	  {
	    		  guiManager.getFileHandler().close();
	    		    
	    		  }
	    		});
	}
	public void visible(boolean b) {
		regex.setText("");
		frame.setVisible(b);
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		
		if(source == menu)
		{
			guiManager.visible(this,false);
			guiManager.visible(guiManager.getData(),true);
		}
		if(source == search)
		{
			 int selection = 0;
			 if(version.isSelected())
			 {
				 selection = 3;
			 }
			 else if(playTime.isSelected())
			 {
				 selection = 4;
			 }
			 else if(release.isSelected())
			 {
				 selection = 5;
			 }
			 else if(name.isSelected())
			 {
				 selection = 0;
			 }
			 else if(comp.isSelected())
			 {
				 selection = 1;
			 }
			 else if(desc.isSelected())
			 {
				 selection = 2;
			 }
			 new ShowAllGui(guiManager.getFileHandler().search_regex_all(regex.getText(), selection));
			
		}
		
	}

}
