package gui;
import classStructure.Record;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class ShowAllGui {
	
	ShowAllGui(ArrayList<Record> list)
	{
	ShowData model = new ShowData(list);
    JTable table = new JTable();
    table.setModel(model);

    JFrame frame = new JFrame();
    frame.add(new JScrollPane(table));
    frame.pack();
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.setVisible(true);    
    
	}
	
	

}
