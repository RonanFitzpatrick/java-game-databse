package gui;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;
import classStructure.Record;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

public class ShowData extends AbstractTableModel

{

	List<Record> data;
	String[] columnNames = {"Name", "Company", "Description","Avg PlayTime",
			"Release Year","Version","Sony","Microsoft","Nintendo"};
	

	public ShowData(ArrayList<Record> data) 
	{
		this.data = data;

	}

		@Override
		public int getRowCount() {
			// TODO Auto-generated method stub
			return data.size();
		}

		@Override
		public Object getValueAt(int arg0, int arg1) 
		{
			String val = "";
			switch(arg1)
			{
			case 0:
				val = data.get(arg0).getName();
				break;
			case 1:
				val = data.get(arg0).getCompany();
				break;
			case 2:
				val += data.get(arg0).getDescription();
				break;
			case 3:
				val += data.get(arg0).getAverage_playtime();
				break;
			case 4:
				val += data.get(arg0).getRealease_year();
				break;
			case 5:
				val += data.get(arg0).getVersion();
				break;
			case 6:
				val += data.get(arg0).isSony();
				break;
			case 7:
				val += data.get(arg0).isMicrosoft();
				break;
			case 8:
				val += data.get(arg0).isNintendo();
				break;
		
			
			
			}
			
			
			return val;
		}

		@Override
		public int getColumnCount() {
			// TODO Auto-generated method stub
			return 9;
		}
		@Override
		 public String getColumnName(int column) {
		        return columnNames[column];
		    }

	}
