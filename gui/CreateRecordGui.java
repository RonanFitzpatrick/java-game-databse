package gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;












import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import classStructure.Record;


// An AWT GUI program inherits from the top-level container java.awt.Frame
public class CreateRecordGui extends Frame implements ActionListener,Ivisible {
	// This class acts as KeyEvent Listener
	private GuiManager guiManager;
	private JTextField name,company,release,playtime,version;  // single-line TextField to receive tfInput key
	private JTextArea desc; // multi-line TextArea to taDisplay result
	private JButton submit,reset,menu;
	private JFrame frame;
	private ButtonGroup sony,micro,nin;
	private JRadioButton sonyF,sonyT,microF,microT,ninF,ninT;
	private JScrollPane sp;
	/** Constructor to setup the GUI */
	public CreateRecordGui(GuiManager guiManager) 
	{
		this.guiManager = guiManager;
		//sets the container for all elements
		frame = new JFrame("FrameDemo");
		frame.setLayout(new FlowLayout()); 
		//adds label
		frame.add(new JLabel("<html>Title of game:<br></html> ", SwingConstants.CENTER));
		//adds text field for the game title(the key)
		name = new JTextField(60);
		frame.add(name);
		//label
		frame.add(new JLabel("<html>Company Name:<br></html> ", SwingConstants.CENTER));
		//adds text field for company
		company = new JTextField(60);
		frame.add(company);
		//label
		frame.add(new JLabel("<html><br>Description</html>", SwingConstants.CENTER));
		//text area for the description
		desc = new JTextArea(5, 60);
		//creates a scrollpane out of description, to allow scrolling of the text
		//in the description text area
		sp = new JScrollPane(desc);
		frame.add(sp); 
		//label
		frame.add(new JLabel("<html>Year of release:</html> ", SwingConstants.CENTER));
		//text field for release year
		release = new JTextField(10);
		frame.add(release);
		//label
		frame.add(new JLabel("<html>Average Playtime in hours:<br></html> ", SwingConstants.CENTER));
		//text field for playtime
		playtime = new JTextField(10);
		frame.add(playtime);
		//label
		frame.add(new JLabel("<html>Version:<br></html> ", SwingConstants.CENTER));
		//version text field
		version= new JTextField(10);
		frame.add(version);
		//create true and false radio buttons for sony
		sonyT = new JRadioButton("True");
		sonyT.setSelected(true);
		sonyF = new JRadioButton("False");
		//creates true and false radio button for microsoft
		microT = new JRadioButton("True");
		microT.setSelected(true);
		microF = new JRadioButton("False");
		//creates true and false radio button for nintendo
		ninT = new JRadioButton("True");
		ninT.setSelected(true);
		ninF = new JRadioButton("False");
		
		//creates the sony button group so they relate logically
		sony = new ButtonGroup();
		sony.add(sonyF);
		sony.add(sonyT);
		//creates microsoft button group so they relate logically
		micro = new ButtonGroup();
		micro.add(microF);
		micro.add(microT);
		//creates nintendo button group so they relate logically
		nin = new ButtonGroup();
		nin.add(ninF);
		nin.add(ninT);
		 frame.addWindowListener(new WindowAdapter() {
	    	  public void windowClosing(WindowEvent e) 
	    	  {
	    		  guiManager.getFileHandler().close();
	    		    
	    		  }
	    		});



		//adds  buttons and labels to the frame
		frame.add(new JLabel("<html>Available sony<br></html> ", SwingConstants.CENTER));
		frame.add(sonyF);
		frame.add(sonyT);

		frame.add(new JLabel("<html>Available microsoft<br></html> ", SwingConstants.CENTER));
		frame.add(microF);
		frame.add(microT);

		frame.add(new JLabel("<html>Available nintendo<br></html> ", SwingConstants.CENTER));
		frame.add(ninF);
		frame.add(ninT);

		//creates all buttons for the different functions

		reset = new JButton("reset");
		reset.addActionListener(this);
		frame.add(reset);
		submit = new JButton("Submit");
		submit.addActionListener(this);
		frame.add(submit);
		menu = new JButton("menu");
		menu.addActionListener(this);
		frame.add(menu);
		//Disallows resizing to keep elements in place
		frame.setResizable(false);
		//sets title
		frame.setTitle("Classic Game Database"); 
		//sets size
		frame.setSize(700, 500);     
		//sets visibility
		frame.setVisible(false);         
	}

	//changes visibility of this frame
	public void visible(boolean b) {
		frame.setVisible(b);
		
	}

	
	@Override
	public void actionPerformed(ActionEvent act) 
	{

		//creates and object to compare to different sourcesof events
		Object source = act.getSource();
		if (source == submit)
		{
			//validate + create
			if(utility.GuiUtility.validAll(name.getText(), company.getText(),desc.getText(), version.getText(), playtime.getText(), release.getText()))
			{
				//checks if a file already exits with that name
				if(!guiManager.getFileHandler().contains_name(name.getText()))
				{
					//parse all the numbers to correct type to put into a record
					short yearVal = (short)Integer.parseInt(release.getText());
					short playTimeVal = (short)Integer.parseInt(playtime.getText());
					double vers = Double.parseDouble(version.getText());
					
					//creates a new record and writes it to file
					guiManager.getFileHandler().add(new Record(
							name.getText(), desc.getText(),company.getText(),
							yearVal, playTimeVal,sonyT.isSelected(),ninT.isSelected(),microT.isSelected(),
							 vers));
					//tells user that the record is added
					JOptionPane.showMessageDialog(frame, "Record added");
					//changes back to data methods
					guiManager.visible(this,false);
					guiManager.visible(guiManager.getData(),true);
					reset();
					


				}
				else
				{
					//tells them there is a record that mathces the key
					JOptionPane.showMessageDialog(frame, "A record with this name "
							+ "is already in this File");
				}

			}
			else
			{
				//tells the user that their entries do not match what is required
				JOptionPane.showMessageDialog(frame, "Name and description must be less than 32 characters\n"
						+ "description must be less than 1000\n"
						+ "Version must be in the format x.x up to 3 decimal places\n"
						+ "year must be 4 numbers\n"
						+ "Playtime must be between 1 and 3 numbers\n");


			}


		}
		else if(source == reset)
		{
			//resets all text fields to blank on users request
			reset();

		}
		else if(source == menu)
		{
			//takes user back to menu
					
			guiManager.visible(this,false);
			guiManager.visible(guiManager.getData(),true);
			reset();
		}

	}
	
	private void reset()
	{
		name.setText("");
		company.setText("");
		release.setText("");
		version.setText("");
		playtime.setText("");
		desc.setText("");
	}


}