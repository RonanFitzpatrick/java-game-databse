package gui;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class DeleteRecordGui extends Frame implements ActionListener,Ivisible
{
	
	 private JButton delete,menu;
	 private JTextField recordName;
	 private JFrame frame;
	 private GuiManager guiManager;
	 public DeleteRecordGui(GuiManager guiManager) 
	 {
		 this.guiManager = guiManager;
		 //container for gui elements
		 frame = new JFrame("FrameDemo");
	     frame.setLayout(new FlowLayout()); 
	     //labybel
	     frame.add(new JLabel("<html>Record Name:<br></html> ", SwingConstants.CENTER));
	    //field that takes in the name of record
	     recordName = new JTextField(20);
	     frame.add(recordName);
	     //buttons
	     menu = new JButton("Menu");
	     menu.addActionListener(this);
	     frame.add(menu);
	     delete = new JButton("Delete");
	     delete.addActionListener(this);
	     frame.add(delete);
	     
	     //stops resizing
	     frame.setResizable(false);
	     //gives a title
	     frame.setTitle("Classic Game Database"); 
	     //sets size of window
	     frame.setSize(400, 400); 
	     //makes Invisible
	     frame.setVisible(false);  
	     
	 }
	//method of invisibility
	

	@Override
	public void actionPerformed(ActionEvent act) 
	{
		//makes object that allows comparison to check against source of event
		Object source = act.getSource();
		if(source == delete)
		{
			//check  if the recordName is valid
			if(utility.GuiUtility.validName(recordName.getText()))
			{
				//trys to delete and checks if record is  deleted
				if(guiManager.getFileHandler().delete(recordName.getText()))
				{
					//informs user record is deleted 
					JOptionPane.showMessageDialog(frame, "Record Deleted");
					//switches back to main
					guiManager.visible(this,false);
					guiManager.visible(guiManager.getData(),true);
					
				}
				else
				{
					//tells users record failed to delete
					JOptionPane.showMessageDialog(frame, "Record failed to delete"
							+ ",it may not exist");
				}
			}
			else
			{
				//tells user the file name they enetered is invalid
				JOptionPane.showMessageDialog(frame, "File name must be between 1-32"
						+ "characters");
			}
		}
		if(source == menu )
		{
			//switches back to data methods
			guiManager.visible(this,false);
			guiManager.visible(guiManager.getMain(),true);
		}
		
	}
	@Override
	public void visible(boolean b) {
		recordName.setText("");
		frame.setVisible(b);
		
	}

}
