package gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;










import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import classStructure.Record;

 

public class EditRecordGui extends Frame implements ActionListener, FocusListener,Ivisible{
      
 
   private JTextField name,company,release,playtime,version,recordNumber; 
   private JTextArea desc; 
   private JButton submit,reset,menu;
   private JFrame frame;
   private ButtonGroup sony,micro,nin;
   private JRadioButton sonyF,sonyT,microF,microT,ninF,ninT;
   private JScrollPane sp;
   private GuiManager guimanager;
   
   public EditRecordGui(GuiManager guimanager) {
	  this.guimanager = guimanager;
      //container for gui elements
      frame = new JFrame("FrameDemo");
      frame.setLayout(new FlowLayout()); 
      //label
      frame.add(new JLabel("<html>Record Name<br></html> ", SwingConstants.CENTER));
      //puts in a recordName field for user to enter name of file they wish to edit
      recordNumber = new JTextField(60);
      recordNumber.addFocusListener(this);
      frame.add(recordNumber);
      //label
      frame.add(new JLabel("<html>Title of game:<br></html> ", SwingConstants.CENTER));
     //adds a field for name to be changed
      name = new JTextField(60);
      frame.add(name);
      //label
      frame.add(new JLabel("<html>Company Name:<br></html> ", SwingConstants.CENTER));
      //adds company field
      company = new JTextField(60);
      frame.add(company);
      //label
      frame.add(new JLabel("<html><br>Description</html>", SwingConstants.CENTER));
     //add description text area
      desc = new JTextArea(5, 60);
      //make description field scrollable
      sp = new JScrollPane(desc);
      frame.add(sp); 
      //LABEL
      frame.add(new JLabel("<html>Year of release:</html> ", SwingConstants.CENTER));
      //release year field 
      release = new JTextField(10);
      frame.add(release);
      //label
      frame.add(new JLabel("<html>Average Playtime in hours:<br></html> ", SwingConstants.CENTER));
      //field for average playtime
      playtime = new JTextField(10);
      frame.add(playtime);
      //label
      frame.add(new JLabel("<html>Version:<br></html> ", SwingConstants.CENTER));
      version= new JTextField(10);
      frame.add(version);
      
      //sony buttons
      sonyT = new JRadioButton("True");
      sonyT.setSelected(true);
      sonyF = new JRadioButton("False");
      
      //microsoft buttons
      microT = new JRadioButton("True");
      microT.setSelected(true);
      microF = new JRadioButton("False");
      
      //nintendo buttons
      ninT = new JRadioButton("True");
      ninT.setSelected(true);
      ninF = new JRadioButton("False");
      
      //sony button groups (connects them logically eg only 1 true)
      sony = new ButtonGroup();
      sony.add(sonyF);
      sony.add(sonyT);
      
      //microsoft button groups (connects them logically eg only 1 true)
      micro = new ButtonGroup();
      micro.add(microF);
      micro.add(microT);
      
      //nintendo button groups (connects them logically eg only 1 true)
      nin = new ButtonGroup();
      nin.add(ninF);
      nin.add(ninT);
      
      
      
      //ADDS ALL BUTTONS AND LABELS
      frame.add(new JLabel("<html>Available sony<br></html> ", SwingConstants.CENTER));
      frame.add(sonyF);
      frame.add(sonyT);
      
      frame.add(new JLabel("<html>Available microsoft<br></html> ", SwingConstants.CENTER));
      frame.add(microF);
      frame.add(microT);
      
      frame.add(new JLabel("<html>Available nintendo<br></html> ", SwingConstants.CENTER));
      frame.add(ninF);
      frame.add(ninT);
      
      
      
      
      
      //adds function buttons
      reset = new JButton("reset"); //submit button
      reset.addActionListener(this);
      frame.add(reset);
      submit = new JButton("Submit");
      submit.addActionListener(this);
      frame.add(submit);
      menu = new JButton("menu");
      menu.addActionListener(this);
      frame.add(menu);
      frame.setResizable(false);//stops resizing
      frame.setTitle("Classic Game Database"); //title of window
      frame.setSize(700, 500);  //size of window       
      frame.setVisible(false);   
      frame.addWindowListener(new WindowAdapter() {
    	  public void windowClosing(WindowEvent e) 
    	  {
    		  guimanager.getFileHandler().close();
    		    
    		  }
    		});

         
	}

		
 
   

@Override

public void actionPerformed(ActionEvent act) 
{
		
		//object that allows comparison to see source of event
	 	Object source = act.getSource();
	 	
	 	
	    if (source == submit)
	    {
	    	
	    	//puts all fields in values
	    	String nameVal = name.getText();
	    	String recVal = recordNumber.getText();
	    	String compVal = company.getText();
	    	String descVal = desc.getText();
	    	String playTimeVal = playtime.getText();
	    	String releaseYearVal = release.getText();
	    	String versionVal = version.getText();
	    	System.out.println(utility.GuiUtility.validAll(nameVal, compVal, descVal, versionVal, playTimeVal, releaseYearVal));
	    	//checks if all fields are valid entries
	    	if(utility.GuiUtility.validAll(nameVal, compVal, descVal, versionVal, playTimeVal, releaseYearVal))
	    	{
	    		
	    		System.out.println("done");
	    		short yearVal = (short)Integer.parseInt(release.getText());
				short playTimes = (short)Integer.parseInt(playtime.getText());
				double vers = Double.parseDouble(version.getText());
				
				//creates a new record and writes it to file
				guimanager.getFileHandler().edit( recVal, (new Record(
						nameVal, descVal,compVal,yearVal, playTimes,
						sonyT.isSelected(),ninT.isSelected(),microT.isSelected(),
						 vers)));
				//goes back to menu

				guimanager.visible(this,false);
				guimanager.visible(guimanager.getData(),true);
				reset();
	    		
	    	}
	    	
	    	
	    	
	    
	    	
	    }
	    else if(source == reset)
	    {
	    	reset();
	    	
	    }
	    else if(source == menu)
	    {
	    	reset();
	    	guimanager.visible(this,false);
			guimanager.visible(guimanager.getData(),true);
	    }
	
}

@Override
public void focusGained(FocusEvent arg0) {
	// TODO Auto-generated method stub
	
}

@Override
public void focusLost(FocusEvent arg0) 
{
	
	if(guimanager.getFileHandler().contains_name(recordNumber.getText()))
	{
		
		  name.setText(guimanager.getFileHandler().search_regex(recordNumber.getText(), 0).getName());
		  company.setText(guimanager.getFileHandler().search_regex(recordNumber.getText(), 0).getCompany());
		  desc.setText(guimanager.getFileHandler().search_regex(recordNumber.getText(), 0).getDescription());
		  version.setText(Double.toString(guimanager.getFileHandler().search_regex(recordNumber.getText(), 0).getVersion()));
		  release.setText(Integer.toString(guimanager.getFileHandler().search_regex(recordNumber.getText(), 0).getRealease_year()));
		  playtime.setText(Integer.toString(guimanager.getFileHandler().search_regex(recordNumber.getText(), 0).getAverage_playtime()));
		  
		 
	}
	else
	{
		JOptionPane.showMessageDialog(frame, "Record does not Exist");		
	}
	//search
}

public void visible(boolean b) {
	frame.setVisible(b);
	
}
private void reset()
{
	recordNumber.setText("");
	name.setText("");
	company.setText("");
	release.setText("");
	version.setText("");
	playtime.setText("");
	desc.setText("");
}
 

}