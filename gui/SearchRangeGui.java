package gui;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class SearchRangeGui extends Frame implements ActionListener, Ivisible
{
	 private JButton search,menu;
	 private JTextField lowRange,highRange;
	 private JFrame frame;
	 private JRadioButton version,playTime,release;
	 private ButtonGroup category;
	 private GuiManager guiManager;
	public SearchRangeGui(GuiManager guiManager)
	{
		this.guiManager = guiManager;
		//container
		  frame = new JFrame("FrameDemo");
	      frame.setLayout(new FlowLayout()); 
	      //lowest value accepted
		  lowRange = new JTextField(10);
		  //highes value accepted
		  highRange = new JTextField(10);
		  //allows user to pick wich field to search
		  version = new JRadioButton("Version",true);
		  playTime = new JRadioButton("AveragePlaytime",false);
		  release = new JRadioButton("Releaseyear" ,false);
		  
		  //logically groups buttons
		  category = new ButtonGroup();
		  category.add(version);
		  category.add(playTime);
		  category.add(release);
		  
		  //adds everything to frame
		  frame.add(new JLabel("Low Range", SwingConstants.CENTER));
		  frame.add(lowRange);
		  frame.add(new JLabel("High Range", SwingConstants.CENTER));
		  frame.add(highRange);
		  frame.add(version);
		  frame.add(playTime);
		  frame.add(release);
		  
		  
	      search= new JButton("search"); //submit button
	      search.addActionListener(this);
	      frame.add(search);
	      menu = new JButton("menu");
	      menu.addActionListener(this);
	      frame.add(menu);
	      
	      frame.setResizable(false);//stops resizing
	      frame.setTitle("Classic Game Database"); //title of window
	      frame.setSize(700, 500);  //size of window       
	      frame.setVisible(false);     
	      frame.addWindowListener(new WindowAdapter() {
	    	  public void windowClosing(WindowEvent e) 
	    	  {
	    		  guiManager.getFileHandler().close();
	    		    
	    		  }
	    		});
	}
	//method of invisibility 
	public void visible(boolean b) {
		frame.setVisible(b);
		
	}
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		//makes object to compare where the event came from
		Object source = e.getSource();
		
		if(source == menu)
		{
			//brings user to menu
			lowRange.setText("");
			highRange.setText("");
			guiManager.visible(this,false);
			guiManager.visible(guiManager.getData(),true);
			
		}
	
		if(source == search)
		{
		 int selected = 3;
		 //sees which to search
		 if(version.isSelected())
		 {
			 selected = 0;
		 }
		 else if(playTime.isSelected())
		 {
			 selected = 1;
		 }
		 else if(release.isSelected())
		 {
			 selected = 2;
		 }
		 new ShowAllGui(guiManager.getFileHandler().search_range(Double.parseDouble(highRange.getText()), Double.parseDouble(lowRange.getText()), selected));
		 
		}
		
	}
	
	

}
