package gui;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class OpenDatabaseGui extends Frame implements ActionListener,Ivisible
{
	 private JButton open,menu;
	 private JTextField dbName;
	 private JFrame frame;
	 private GuiManager  guiManager;
	 
	 public OpenDatabaseGui(GuiManager guiManager) 
	 {
		 //container for gui
		 this.guiManager = guiManager;
		 frame = new JFrame("FrameDemo");
	     frame.setLayout(new FlowLayout()); 
	     //label
	     frame.add(new JLabel("<html>Database Name:<br></html> ", SwingConstants.CENTER));
	     //database name
	     dbName = new JTextField(20);
	     frame.add(dbName);
	     //adds functions buttons
	     menu = new JButton("Menu");
	     menu.addActionListener(this);
	     frame.add(menu);
	     open = new JButton("Open");
	     open.addActionListener(this);
	     frame.add(open);
	     //sets dimensions titles and visiblity
	     frame.setResizable(false);//stops resizing
	     frame.setTitle("Classic Game Database"); //title of window
	     frame.setSize(400, 400);  //size of window       
	     frame.setVisible(false);   
	 }
	 //method of invisibility
	 
		public void visible(boolean b) {
			dbName.setText("");
			frame.setVisible(b);
			
		}

	@Override
	public void actionPerformed(ActionEvent act) {
		//an object to allow comparison of event
		Object source = act.getSource();
		if(source == menu)
		{
			//bringsback to menu
			
			guiManager.visible(this,false);
			guiManager.visible(guiManager.getMain(),true);
			
		}
		if(source == open)
		{
			//make sure file is valid name
			if(utility.GuiUtility.validFile(dbName.getText()))
			{
				//creates a new file
				File file = new File(guiManager.getDatabase()+dbName.getText()+".txt");
				//if file does exist it creates it
				if(file.exists())
				{
				//open file
				guiManager.setOpen(true);
				guiManager.setFileHandler(file);
				guiManager.visible(this,false);
				guiManager.visible(guiManager.getData(),true);
				}
				//tells user file doesn't exist
				else
				{
			 	JOptionPane.showMessageDialog(frame, "DataBase does not exist");
				}
			}
			//tells user the file name is invalid
			else
			{
				JOptionPane.showMessageDialog(frame, "Name must contain at least one"
						+ "Character and must not contain the fullstop character '.'");
			}
		}
	}
}
