package gui;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class DataMethodGui extends Frame implements ActionListener,Ivisible
{
	 private JButton deleteAll,delete,add,edit,showAll,searchRange,searchRegex
	 ,count,menu;
	 private  JFrame frame;
	 private GuiManager guiManager;
	 public DataMethodGui(GuiManager guiManager) 
	 {
		 this.guiManager = guiManager;
		 //creates the container for gui elements
		 frame = new JFrame("FrameDemo");
	     frame.setLayout(new FlowLayout()); 
	     
	     //This is a menu gui all this code are buttons
	      deleteAll = new JButton("Delete All records"); //submit button
	      deleteAll.addActionListener(this);
	      frame.add(deleteAll);
	      delete = new JButton("Delete a record");
	      delete.addActionListener(this);
	      frame.add(delete);
	      
	      add = new JButton("add a record");
	      add.addActionListener(this);
	      frame.add(add);
	      
	      edit = new JButton("Edit record");
	      edit.addActionListener(this);
	      frame.add(edit);
	      
	      showAll = new JButton("Show All Records");
	      showAll.addActionListener(this);
	      frame.add(showAll);
	      
	      searchRange = new JButton("Search by Range");
	      searchRange.addActionListener(this);
	      frame.add(searchRange);
	      
	      searchRegex = new JButton("search by Regex");
	      searchRegex.addActionListener(this);
	      frame.add(searchRegex);
	      
	      count = new JButton("Count of records");
	      count.addActionListener(this);
	      frame.add(count);
	      
	      
	      menu = new JButton("Menu");
	      menu.addActionListener(this);
	      frame.add(menu);
	      //submit button
	      
	     //resize false to stop elements moving
	      
	      frame.setResizable(false);
	      //sets title
	      frame.setTitle("Classic Game Database");
	      //sets size of window
	      frame.setSize(200, 600);
	      //make frame invisible
	      frame.setVisible(false);      
	      frame.addWindowListener(new WindowAdapter() {
	    	  public void windowClosing(WindowEvent e) 
	    	  {
	    		  guiManager.getFileHandler().close();
	    		    
	    		  }
	    		});
	 }
	@Override
	public void actionPerformed(ActionEvent act) 
	{
		//makesan object to compare to get where event comes from
		Object source = act.getSource();
		//deletes all records and informs user they are deleted
		if(source == deleteAll)
		{
			guiManager.getFileHandler().deleteAll();
			JOptionPane.showMessageDialog(frame, "All Records have been deleted");
			
		}
		//brings user to the delete by record window
		if(source == delete)
		{
			guiManager.visible(this,false);
			guiManager.visible(guiManager.getDeleteRec(),true);
			
			
		}
		//brings user to add record window
		if(source == add)
		{
			guiManager.visible(this,false);
			guiManager.visible(guiManager.getRec(),true);
			
		}
		//brings user to edit window
		if(source == edit)
		{
			guiManager.visible(this,false);
			guiManager.visible(guiManager.getEdit(),true);
		
			
		}
		//shows user how many records in a dialog
		if(source == count)
		{
			JOptionPane.showMessageDialog(frame, "There are " + guiManager.getFileHandler().count()
					+ " records in the database");
		}
		//brings user back to database methods
		if(source == menu)
		{
			guiManager.getFileHandler().close();
			guiManager.setOpen(false);
			guiManager.visible(this,false);
			guiManager.visible(guiManager.getMain(),true);
			
			
		}
		if(source == showAll)
		{
			guiManager.showAllRecVis();
		}
	
		//brings user to search by regex screen
		if(source == searchRegex)
		{
			guiManager.visible(this,false);
			guiManager.visible(guiManager.getSearch(),true);
		
			
		}
		//brings user to search a range screen
		if(source == searchRange)
		{
			guiManager.visible(this,false);
			guiManager.visible(guiManager.getSearchRange(),true);
				
		}
		
	}

	
	
	//controls visibility of window
	public void visible(boolean b) {
		frame.setVisible(b);
		
	}

}
