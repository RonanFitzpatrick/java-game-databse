package controller;

import gui.GuiManager;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import classStructure.Record;
import databaseManipulation.DataBaseManipulator;
import databaseManipulation.FileManipulator;

public class MainApp
{
	private final String DEFAULT_DATABASE_DIR = "../databases/";
	private  static final int RECORD_SIZE = 2267;
	public static void main(String[] args)
	{
		MainApp theApp = new MainApp();
		theApp.start();
	}
	
	private void start()
	{
		initial_startup();
		new gui.GuiManager(DEFAULT_DATABASE_DIR);
	}
	
	private void runTests()
	{
		databaseManipulation.DataBaseManipulator.create("Test", DEFAULT_DATABASE_DIR);
		FileManipulator handle = new FileManipulator(new File(DEFAULT_DATABASE_DIR + "/Test.txt"), DEFAULT_DATABASE_DIR, 2267);
		
		
		System.out.println(handle.showAll());
		handle.deleteAll();
		short one  =10;
		
		handle.add(new Record("TEST", "TEST", "TEST", one, one, true, true, false, 1.0));
		System.out.println(handle.showAll());
		handle.close();
	
		System.out.println("Size: " + handle.count());
			
	}
	
	public  void initial_startup()
	{
		
		File database_dir = new File(DEFAULT_DATABASE_DIR);
		File internal_hashmap_dir = new File(DEFAULT_DATABASE_DIR + "/hashmaps");
		File internal_garbagelist_dir = new File(DEFAULT_DATABASE_DIR + "/garbagelists");
		
		
		if(!database_dir.exists())
		{
			database_dir.mkdir();
		}
		if(!internal_hashmap_dir.exists())
		{
			internal_hashmap_dir.mkdirs();
		}
		if(!internal_garbagelist_dir.exists())
		{
			internal_garbagelist_dir.mkdir();
		}
	}	

}