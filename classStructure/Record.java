package classStructure;

public class Record
{
	private String name, description, company;
	private short realease_year, average_playtime;
	private boolean sony, nintendo, microsoft;
	private double version;
	
	/*					String1 String2 String3	 (   primitive data   )	
	 * Size of record = (2*64 + 2 * 64 + 2000) + 2 + 2 + 1 + 1 + 1 + 4
	 */
	
	public static int RECORD_SIZE = 2267;
	
	public Record(String name, String description, String company,
			short realease_year, short average_playtime, boolean sony,
			boolean nintendo, boolean microsoft, double version)
	{
		this.name = name;
		this.description = description;
		this.company = company;
		this.realease_year = realease_year;
		this.average_playtime = average_playtime;
		this.sony = sony;
		this.nintendo = nintendo;
		this.microsoft = microsoft;
		this.version = version;
	}

	public Record()
	{
		
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getCompany()
	{
		return company;
	}

	public void setCompany(String company)
	{
		this.company = company;
	}

	public short getRealease_year()
	{
		return realease_year;
	}

	public void setRealease_year(short realease_year)
	{
		this.realease_year = realease_year;
	}

	public short getAverage_playtime()
	{
		return average_playtime;
	}

	public void setAverage_playtime(short average_playtime)
	{
		this.average_playtime = average_playtime;
	}

	public boolean isSony()
	{
		return sony;
	}

	public void setSony(boolean sony)
	{
		this.sony = sony;
	}

	public boolean isNintendo()
	{
		return nintendo;
	}

	public void setNintendo(boolean nintendo)
	{
		this.nintendo = nintendo;
	}

	public boolean isMicrosoft()
	{
		return microsoft;
	}

	public void setMicrosoft(boolean microsoft)
	{
		this.microsoft = microsoft;
	}

	public double getVersion()
	{
		return version;
	}

	public void setVersion(double version)
	{
		this.version = version;
	}
	
	public boolean testRegex(String regex)
	{
		if(this.name.matches(regex))
		{
			return true;
		}
		if(this.company.matches(regex))
		{
			return true;
		}
		if(this.description.matches(regex))
		{
			return true;
		}
		if(String.valueOf(this.version).matches(regex))
		{
			return true;
		}
		if(String.valueOf(this.average_playtime).matches(regex))
		{
			return true;
		}
		if(String.valueOf(this.realease_year).matches(regex))
		{
			return true;
		}
		
		return false;
	}
	public boolean testRange(double  hi,double low, int val)
	{
		
		if(this.version >= low && this.version <= hi && val == 0)
		{
			return true;
		}
		else if(this.average_playtime >= low && this.average_playtime <= hi && val == 1)
		{
			return true;
		}
		else if(this.realease_year >=low && this.realease_year <= hi && val == 2)
		{
			return true;
		}
		
		return false;
	}
	
	public boolean testRegex(String regex, int val)
	{
		
		//TODO; Change val to enum
		
		if(this.name.matches(regex) && val == 0)
		{
			return true;
		}
		if(this.company.matches(regex) && val == 1)
		{
			return true;
		}
		if(this.description.matches(regex) && val == 2)
		{
			return true;
		}
		if(String.valueOf(this.version).matches(regex) && val == 3)
		{
			return true;
		}
		if(String.valueOf(this.average_playtime).matches(regex) && val == 4)
		{
			return true;
		}
		if(String.valueOf(this.realease_year).matches(regex) && val == 5)
		{
			return true;
		}
		
		return false;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + average_playtime;
		result = prime * result + ((company == null) ? 0 : company.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + (microsoft ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (nintendo ? 1231 : 1237);
		result = prime * result + realease_year;
		result = prime * result + (sony ? 1231 : 1237);
		long temp;
		temp = Double.doubleToLongBits(version);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Record other = (Record) obj;
		if (average_playtime != other.average_playtime)
			return false;
		if (company == null)
		{
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (description == null)
		{
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (microsoft != other.microsoft)
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (nintendo != other.nintendo)
			return false;
		if (realease_year != other.realease_year)
			return false;
		if (sony != other.sony)
			return false;
		if (Double.doubleToLongBits(version) != Double
				.doubleToLongBits(other.version))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "Record [name=" + name + ", description=" + description
				+ ", company=" + company + ", realease_year=" + realease_year
				+ ", average_playtime=" + average_playtime + ", sony=" + sony
				+ ", nintendo=" + nintendo + ", microsoft=" + microsoft
				+ ", version=" + version + "]";
	}
	
}
