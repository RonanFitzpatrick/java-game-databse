package utility;

public class GuiUtility 

{
	
	public static boolean validFile(String file)
	{
		if((!file.contains(".")) && file.length() > 0)
		{
			return true;
		}
		return false;
	}
	
	public static boolean validAll(String name,String company,String desc,String version,String
			playTime, String releaseYear)
	{
		return (validName(name) && validCompany(company)&& validDescription(desc)
				&& validVersion(version) && validPlayTime(playTime) && validReleaseYear(releaseYear));
	}

	public static boolean validName(String name)
	{
		int max = 32;
		int min = 0;
		if (name.length() < max && name.length() > min)
		{
			
		return true;
		}
		return false;
	}
	public static boolean validCompany(String comp)
	{
		int max = 32;
		int min = 0;
		if (comp.length() < max && comp.length() > min)
		{
			
		return true;
		}
		return false;
	}
	public static boolean validDescription(String desc)
	{
		int max = 1000;
		int min = 0;
		
		if (desc.length() < max && desc.length() > min)
		{
		return true;
		}
		return false;
	}
	public static boolean validVersion(String version)
	{
		//double check..
		String regex = "[0-9]{1,3}.[0-9]{1,3}";
		if(version.matches(regex))
		{
			
			return true;
		}
		return false;
	}
	public static boolean validPlayTime(String playTime)
	{	
		
		String rege = "[0-9]{1,3}";
		if(playTime.matches(rege))
		{
			System.out.println("playtime");
			return true;
		}
		return false;
	}
	public static boolean validReleaseYear(String year)
	{
		
		String regex = "[0-9]{4}";
		if(year.matches(regex))
		{
			System.out.println("year");
			return true;
		}
		return false;
	}
	


}
