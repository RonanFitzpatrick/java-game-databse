package utility;

public class StringUtility 
{
	public static final char Pad_Char = '�';
	
	public static String pad(String data, int pad_length)
	{
		if(data == null)
			return null;
		
		int paddingRequired = pad_length - data.length();
		
		if(paddingRequired < 0)
		{
			return null; 
		}	
		else if(paddingRequired == 0)
		{
			return data;
		}
		
		StringBuilder strBuilder = new StringBuilder(data);
		
		for(int i = 0; i < paddingRequired; i++)
		{
			strBuilder.append(Pad_Char);
		}
		
		return strBuilder.toString();
	}
	
	public static String unpad(String data, int pad_length) //John1234
	{
		if(data == null)
		{
			return null;
		}
			
		int indexPadChar = data.indexOf(Pad_Char);

		if((indexPadChar == -1) && (data.length() == pad_length))
		{
			return data;
		}
		else if(indexPadChar == -1)
		{
			return null;
		}
			
		return data.substring(0, indexPadChar);
	}
}



















