package databaseManipulation;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;

import classStructure.Record;

public class FileManipulator
{
	private RandomAccessFile database_file;
	private File database_file_reference;
	private HashMap<String, Integer> internal_map;
	private ArrayList<Integer> garbageList;
	
	private int record_size;
	private int record_count;
	
	private String database_dir;
	private String name;
	
	private static final int NUM_SKIP = 4;
	
	
	@SuppressWarnings("unchecked")
	public FileManipulator(File database_file_reference, String database_dir, int record_size) 
	{
		String cut_name = database_file_reference.getName().substring(0, database_file_reference.getName().length() - 4);
		
		this.name = cut_name;
		this.database_dir = database_dir;
		this.record_size = record_size;
		this.database_file_reference = database_file_reference;
		this.database_file = DataBaseManipulator.Open(cut_name, database_dir);
		
		try
		{
			if(this.database_file.length() == 0)
			{
				this.record_count = 0;
				write_header(0);
			}
			else
			{
				this.record_count = read_header();
			}
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		Object temp_obj1 = utility.SerializationUtility.load(database_dir, "hashmaps/" + cut_name);
		
		if(temp_obj1 instanceof HashMap)
		{
			this.internal_map = (HashMap<String, Integer>) temp_obj1;
		}
		
		Object temp_obj2 = utility.SerializationUtility.load(database_dir, "garbagelists/" + cut_name);
		
		if(temp_obj2 instanceof ArrayList)
		{
			this.garbageList = (ArrayList<Integer>) temp_obj2;
		}
		
	}
	
	/*
	 * What this method does is it increments the record count right away. 
	 * 
	 * It then proceeds to write the updated record count to the start of the file.
	 * Once done it checks if there are any deleted records in the garbage list and
	 * then it proceeds to write over them if there are.
	 * 
	 * IE -- If the list is empty, there are no deleted records and it will just
	 * write to the end of the file.
	 */
	public boolean add(Record record)
	{
		if(!contains_name(record.getName()))
		{
			this.record_count++;
			long pos = NUM_SKIP;
			int index = 0;
			write_header(this.record_count);
			
			if(!this.garbageList.isEmpty())
			{	
				index = garbageList.get(0);
				pos += index * this.record_size; //There is deleted data.
				
				garbageList.remove(0);
				garbageList.trimToSize();
			}
			else
			{
				//The -1 here allows for the record_count to be incremented right away.
				index = this.record_count - 1;
				pos += (index * this.record_size); //There is no deleted data.
			}
			
			write(record, pos);
			this.internal_map.put(record.getName(), index);
			
			return true;
		}
		
		return false;
	}
	public boolean edit(String name, Record record)
    {
            if(contains_name(name))
            {
                    int size = (this.internal_map.get(name) * this.record_size) + NUM_SKIP;
                    write(record, (size));
                    this.internal_map.put(record.getName(), size);
                    this.internal_map.remove(name);
                    return true;
            }
           
            return false;
    }
	
	public boolean delete(String name)
	{	
		if(contains_name(name))
		{
			this.garbageList.add(this.internal_map.get(name));
			this.internal_map.remove(name);
			
			this.record_count--;
			write_header(this.record_count);
			
			return true;
		}
		
		return false;
	}
	
	public void deleteAll()
	{
		this.record_count = 0;
		this.internal_map.clear();
		this.garbageList.clear();
		write_header(NUM_SKIP);
		
		try
		{
			this.database_file_reference.delete();
			this.database_file_reference.createNewFile();
			this.database_file = new RandomAccessFile(this.database_file_reference, "rw");
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void close()
	{
		utility.SerializationUtility.save(this.database_dir + "/hashmaps/", this.name, this.internal_map);
		utility.SerializationUtility.save(this.database_dir + "/garbagelists/", this.name, this.garbageList);
	}
	
	/*
	 * This simply runs a regex check and returns the
	 * first record that matches the data. Possible conversion to
	 * return all data.
	 * 
	 * The second method uses an int to check against a single piece of data.
	 * 
	 * The number match ups can be found at the bottom of the Record class.
	 * This will be swapped to an Enum later.
	 */
	public ArrayList<Record> search_regex_all(String str_regex)
	{
		ArrayList<Record> returnList = new ArrayList<Record>();
		
		Record record = null;
		for(int i = 0; i < this.record_count; i++)
		{
			record=getData(i);
			
			if(record.testRegex(str_regex))
			{
				returnList.add(record);
			}
		}
		
		return returnList;
	}
	
	public ArrayList<Record> search_regex_all(String str_regex,int param)
	{
		ArrayList<Record> returnList = new ArrayList<Record>();
		
		Record record = null;
		for(int i = 0; i < this.record_count; i++)
		{
			record=getData(i);
			
			if(record.testRegex(str_regex,param))
			{
				returnList.add(record);
			}
		}
		
		return returnList;
	}
	
	public Record search_regex(String str_regex)
	{
		Record record = null;
		
		for(int i = 0; i < this.record_count; i++)
		{
			record = getData(i);
			
			if(record.testRegex(str_regex))
			{
				return record;
			}
		}
		
		return null;
	}
	public Record search_regex(String str_regex, int param)
	{
		Record record = null;
		
		for(int i = 0; i < this.record_count; i++)
		{
			record = getData(i);
			
			if(record.testRegex(str_regex, param))
			{
				return record;
			}
		}
		
		return null;
	}
	
	/*
	 * Will need to change the params to suit doubles.
	 */
	public ArrayList<Record> search_range(double high, double low, int param)
	{
		ArrayList<Record> returnList = new ArrayList<Record>();
		
		Record record = null;
		for(int i = 0; i < this.record_count; i++)
		{
			record=getData(i);
			if(record.testRange(high,low,param))
			{
				
				returnList.add(record);
			}
		}
		
		return returnList;
	}
	
	public ArrayList<Record> showAll()
	{
		ArrayList<Record> return_list = new ArrayList();
		/*
		for(String temp : this.internal_map.keySet())
		{
			return_list.add(search_regex(temp,0));
		}*/
		for(int i = 0; i < count(); i++)
		{
			return_list.add(getData(i));
		}
	
		return return_list;
	}
	
	public int count()
	{
		return this.record_count;
	}
	
	public Record getData(int curr_count)
	{
		Record data = new Record();
		
		try
		{
			this.database_file.seek((curr_count * this.record_size ) + NUM_SKIP);
			
			data.setName(utility.StringUtility.unpad(this.database_file.readUTF(),64));
			data.setCompany(utility.StringUtility.unpad(this.database_file.readUTF(),64));
			data.setDescription(utility.StringUtility.unpad(this.database_file.readUTF(),1000));
			data.setRealease_year(this.database_file.readShort());
			data.setAverage_playtime(this.database_file.readShort());
			data.setSony(this.database_file.readBoolean());
			data.setNintendo(this.database_file.readBoolean());
			data.setMicrosoft(this.database_file.readBoolean());
			data.setVersion(this.database_file.readDouble());
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		return data;
	}
	public Record getData(String name)
	{
		Record data = new Record();
		
		if(this.contains_name(name))
		{
			try
			{
				this.database_file.seek(NUM_SKIP + (this.internal_map.get(name) * this.record_size));
				
				data.setName(utility.StringUtility.unpad(this.database_file.readUTF(),64));
				data.setCompany(utility.StringUtility.unpad(this.database_file.readUTF(),64));
				data.setDescription(utility.StringUtility.unpad(this.database_file.readUTF(),1000));
				data.setRealease_year(this.database_file.readShort());
				data.setAverage_playtime(this.database_file.readShort());
				data.setSony(this.database_file.readBoolean());
				data.setNintendo(this.database_file.readBoolean());
				data.setMicrosoft(this.database_file.readBoolean());
				data.setVersion(this.database_file.readDouble());
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
			
		}
		
		return data;
	}
	
	public boolean contains_name(String name)
	{
		return this.internal_map.containsKey(name);
	}
	
	/* Start of private methods */
	
	private void write_header(int size)
	{
		try
		{
			this.database_file.seek(0);
			this.database_file.writeInt(size);
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
	}
	
	private int read_header()
	{
		try
		{
			this.database_file.seek(0);
			return this.database_file.readInt();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return -1;
	}
	
	private void write(Record record, long pos)
	{
		try
		{
			this.database_file.seek(pos);
			
			database_file.writeUTF(utility.StringUtility.pad(record.getName(),64));
			database_file.writeUTF(utility.StringUtility.pad(record.getCompany(),64));
			database_file.writeUTF(utility.StringUtility.pad(record.getDescription(),1000));
			database_file.writeShort(record.getRealease_year());
			database_file.writeShort(record.getAverage_playtime());
			database_file.writeBoolean(record.isSony());
			database_file.writeBoolean(record.isNintendo());
			database_file.writeBoolean(record.isMicrosoft());
			database_file.writeDouble(record.getVersion());
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
