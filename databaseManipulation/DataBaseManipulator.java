package databaseManipulation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;

public class DataBaseManipulator
{
	private static StringBuilder builder;
	
	/*
	 * @param name  The name of the database file to create.
	 * 
	 * This method builds both the serialised hashmap and the database file itself.
	 * It does not open it, all it does is simply write the templated files to disk
	 * with the names provided.
	 */
	public static boolean create(String name, String path)
	{
		
		if(name.length() > 0 && (!name.contains(".")))
		{
			
			String name_extension = getName(name, path);
			File database_file = new File(name_extension);
			
			HashMap<String, Integer> save_map = new HashMap<String, Integer>();
			ArrayList<Integer> garbage_list = new ArrayList<Integer>();
			
			if(database_file.exists())
			{
				return false;
			}
			else
			{
				try
				{
					database_file.createNewFile();
					utility.SerializationUtility.save(path + "/hashmaps/", name, save_map);
					utility.SerializationUtility.save(path + "/garbagelists/", name, garbage_list);
					
					return true;
				}
				catch(IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		return false;
	}
	
	/*
	 * @param name The name of the file that is being opened.
	 * 
	 * This searches the database directory for the file name given.
	 * If this file is not found it will commence some error handling
	 * and prompt the user to enter another file name.
	 */
	public static RandomAccessFile Open(String name, String path)
	{		
		File file = new File(getName(name, path));
		
		if(file.exists())
		{
			RandomAccessFile data;
			
			try
			{
				data = new RandomAccessFile(file, "rw");
				return data;
			} 
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	/*
	 * @param name The name of the file to be deleted.
	 * 
	 * This method searches the database for the file which will be deleted.
	 * If it is found, it will be deleted and true will be returned. Otherwise
	 * nothing will be done and it will return false.
	 */
	
	public static boolean Delete(String name, String path)
	{
		File file = new File(getName(name, path));
		
		if(file.exists())
		{
			return file.delete();
		}
		
		return false;
	}
	
	/*
	 * @param name The name of the file to be appended to.
	 * 
	 * Local private method that converts the name to have the .txt
	 * file extension.
	 */
	private static String getName(String name, String path)
	{
		builder = new StringBuilder();
		builder.append(path);
		builder.append(name);
		builder.append(".txt");
		return builder.toString();
	}
	
	
}
