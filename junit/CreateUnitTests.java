package junit;


import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CreateUnitTests extends TestCase
{
	
	public CreateUnitTests(String testName)
	{
		super(testName);
	}

	@Before
	public void setUp() throws Exception 
	{
		
	}

	@After
	public void tearDown() throws Exception 
	{
		
	}

	@Test
	public void testCreate() 
	{
		File file;
		databaseManipulation.DataBaseManipulator.create("tests", "../databases/");
		file = new File("../databases/tests.txt");
		assertEquals(true,file.exists());
	}
	
	public void testCreate_NotFound()
	{
		
		File file;
		databaseManipulation.DataBaseManipulator.create("..tests", "../databases/");
		file = new File("../databases/..tests.txt");
		assertEquals(false,file.exists());
	}
	
	public void testCreateUnique()
	{
		
		File file;
		
		file = new File("../databases/alreadyCreated.txt");
		try
		{
			file.createNewFile();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(false,databaseManipulation.DataBaseManipulator.create("alreadyCreated", "../databases/"));
	}
	
	public void testOpen()
	{
		databaseManipulation.DataBaseManipulator.create("tests", "../databases/");
		
		assertNotNull(databaseManipulation.DataBaseManipulator.Open("tests", "../databases/")); 
		
	}
	
	public void testOpen_NotFound()
	{
		
		assertNull(databaseManipulation.DataBaseManipulator.Open("notARealFile", "../databases/"));
	}
	
	public void testDelete()
	{
		databaseManipulation.DataBaseManipulator.create("testdelete", "../databases/");
		databaseManipulation.DataBaseManipulator.Delete("testdelete", "../databases/");
		File f = new File("../databases/testdelete.txt");
		assertEquals(false,f.exists());
	}
	
	public void testDelete_notFound()
	{
		
		assertEquals(false,databaseManipulation.DataBaseManipulator.Delete("testdeletenotreal", "../databases/"));
	}
	
	
	
	


}
