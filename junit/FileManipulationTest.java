package junit;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import classStructure.Record;
import databaseManipulation.FileManipulator;

public class FileManipulationTest extends TestCase
{
	public FileManipulationTest(String testName)
	{	
		super(testName);
		
		
	}

	private FileManipulator handle;
	@Before
	public void setUp() throws Exception
	{
		FileManipulationTest test = new FileManipulationTest("testAdd");
		databaseManipulation.DataBaseManipulator.create("testMethods","../databases/");
		handle =new FileManipulator(new File("../databases/" + "/testMethods.txt"), "../databases/", 2267);
	}
	

	@After
	public void tearDown() throws Exception
	{
		handle.close();
	}

	@Test
	public void testAdd() 
	{
		
		
		short one = 1;	
		Record r = new Record("TEST", "TEST", "TEST", one, one, true, true, false, 1.0);
		handle.add(r);
		
		assertNotNull(handle.search_regex("TEST"));
	}
	
	
	public void testAddUnique() 
	{
		short one = 1;
		Record r = new Record("TEST1", "TEST", "TEST", one, one, true, true, false, 1.0);
		handle.add(r);
		assertEquals(false,handle.add(r));
	}
	

	public void testEdit()
	{
		
		short one = 1;
		Record r = new Record("TEST2", "TEST", "TEST", one, one, true, true, false, 1.0);
		Record r2 = new Record("TEST3", "TEST", "TEST", one, one, true, true, false, 1.0);
		handle.deleteAll();
		handle.add(r);
		
		handle.edit("TEST2", r2);
		
		boolean check = r2.toString().equals(handle.search_regex("TEST3").toString());
		
		assertEquals(true,check);
	}
	
	
	public void testEdit_NotFound()
	{
		short one = 1;
		Record r2 = new Record("TEST3", "TEST", "TEST", one, one, true, true, false, 1.0);
		assertEquals(false,handle.edit("TESTwarbleegable", r2));
	}
	
	public void testRegex()
	{
		short one = 1;
		handle.deleteAll();
		Record r2 = new Record("TEST3", "TEST", "TEST", one, one, true, true, false, 1.0);
		handle.add(r2);
		boolean check = r2.toString().equals(handle.search_regex("(T|t)(E|e)(S|s)(T|t)",1).toString());
		assertEquals(true,check);
		
	}
	
	public void testRegex_notFound()
	{
		handle.deleteAll();
		assertNull(handle.search_regex("sdfadsfasda",1));
	}
	
	public void testRange()
	{
		handle.deleteAll();
		short one = 1;
		Record r2 = new Record("TEST3", "TEST", "TEST", (short)2007, one, true, true, false, 1.0);
		handle.add(r2);
		
		Record r3 = new Record("TEST4", "TEST", "TEST", (short)2012, one, true, true, false, 1.0);
		
		handle.add(r3);
		boolean check = r2.toString().equals(handle.search_range(2007, 2006, 2).get(0).toString());
		assertEquals(true,check);
		
		
	}
	
	public void testRange_NotFound()
	{
		handle.deleteAll();
		short one = 1;
		Record r2 = new Record("TEST3", "TEST", "TEST", (short)2007, one, true, true, false, 1.0);
		handle.add(r2);

		assertEquals(true,handle.search_range(5, 3, 2).isEmpty());
			
	}
	
	public void testDelete() 
	{
		
		handle.deleteAll();
		short one = 1;
		Record r = new Record("TEST568", "TEST", "TEST", one, one, true, true, false, 1.0);
		handle.add(r);
		handle.delete("TEST568");
		assertEquals(false,handle.showAll().contains(r));
	}
	
	public void testDelete_NotFound() 
	{
		short one = 10;
		Record r = new Record("TEST", "TEST", "TEST", one, one, true, true, false, 1.0);
		handle.add(r);
		handle.deleteAll();
		assertEquals(false,handle.delete("TEST"));
	}
	
	public void testDeleteAll()
	{
		short one = 10;
		Record r = new Record("TEST", "TEST", "TEST", one, one, true, true, false, 1.0);
		handle.add(r);
		File f = new File("../databases//testMethods.txt");
		handle.deleteAll();
		assertEquals(true, (f.exists() && handle.count() == 0));
	}
	
	public void testShowAll()
	{
		handle.deleteAll();
		short one = 10;
		ArrayList<Record> list = new ArrayList<Record>();
		Record r = new Record("TEST", "TEST", "TEST", one, one, true, true, false, 1.0);
		handle.add(r);
		Record r2 = new Record("TEST1", "TEST", "TEST", one, one, true, true, false, 1.0);
		handle.add(r2);
		boolean check = (r.toString()+r2.toString()).equals((handle.showAll().get(0).toString()+handle.showAll().get(1).toString()));
		assertEquals(true,check);
		
	}
	public void testShowAll_NoData()
	{
	handle.deleteAll();
	assertEquals(true,(handle.showAll().isEmpty()));
	}
}
